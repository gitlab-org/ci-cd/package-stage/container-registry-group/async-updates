# Container Registry Async Updates

Project to hold issues and provide regular async updates for the [Container Registry Group](https://about.gitlab.com/handbook/engineering/development/ops/package/container-registry/)

https://about.gitlab.com/handbook/engineering/development/ops/#async-updates-no-status-in-meetings
