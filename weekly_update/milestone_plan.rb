# Represents the team's planning issue for the current milestone
class MilestonePlan
  attr_reader :gitlab_api, :today

  def initialize(gitlab_api, today)
    @gitlab_api = gitlab_api
    @today = today
  end

  # Group by epic
  def by_feature
    @by_feature ||= milestone_issues.group_by(&:feature)
  end

  def url
    @url ||= planning_issue&.web_url
  end

  def milestone
    @milestone ||= JankyCache.fetch("milestone-#{today}") do
      gitlab_api
        .group_milestones("gitlab-org", state: :active, per_page: 100)
        .find do |details|
          details.due_date &&
          details.start_date &&
          today <= Date.parse(details.due_date) &&
          today >= Date.parse(details.start_date)
        end
      end
  end

  def milestone_issues
    @milestone_issues ||= [].tap { fetch_milestone_issues(_1) }
  end

  def planning_issue
    @planning_issue ||= current_planning_issue || upcoming_planning_issue
  end

  def rollover_issues
    @rollover_issues ||= Team.all.flat_map do |username|
      JankyCache.fetch("rollover-#{milestone.id}-#{username}") do
        gitlab_api.issues(
          nil, {
          not: { milestone: milestone.title, labels: ['Planning Issue'] },
          state: :opened,
          assignee_username: username,
          milestone_id: :Started,
          scope: :all,
        })
      end
    end
  end

  private

  def fetch_milestone_issues(collector)
    gitlab_api.group_issues(
      "gitlab-org",
      milestone: milestone.title,
      labels: [Team.group],
      not: { labels: ['Planning Issue', 'OpsSection::Weekly-Update'] }
    ).each_page do |page|
      page.each do |details|
        collector << Issue.new(details).tap { _1.merge_requests = fetch_related_mrs(_1) }
      end
    end
  end

  # This info isn't batch-able, so we have to call the endpoint for each issue :(
  def fetch_related_mrs(issue)
    puts "Fetching related MRs for issue ##{issue.id}..."

    JankyCache.fetch("/projects/#{issue.project_id}/issues/#{issue.iid}/related_merge_requests") { gitlab_api.get(_1) }
  end

  def current_planning_issue
    gitlab_api.issues(
      Team.planning_issue_project,
      labels: ['Planning Issue', Team.group],
      search: milestone.title,
      in: "title",
    ).first
  end

  def upcoming_planning_issue
    gitlab_api.issues(
      Team.planning_issue_project,
      labels: ['Planning Issue', Team.group],
      in: "title",
      state: "opened",
    ).first
  end
end