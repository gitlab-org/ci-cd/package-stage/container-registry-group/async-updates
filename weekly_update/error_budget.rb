# Queries public snapshot of Grafana chart for team availability
class ErrorBudget
  def availability
    @availability ||= begin
      response = HTTParty.get(Team.availability_url)

      JSON.parse(response.body)
    rescue StandardError
      puts 'Failed to retrieve error budget details. Got response:'
      puts response.code
      puts response.body
    end
  end

  def status
    return "⚠️ " unless availability&.is_a?(Float)
    return "🟢" if availability >= 0.9997
    return "🟡" if availability >= 0.9995
    "🔴"
  end
end