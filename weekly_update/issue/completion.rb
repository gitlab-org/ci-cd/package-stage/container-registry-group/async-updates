module IssueCompletion
  def completion_ratio
    @ratio ||= begin
      return 1.0 if likely_complete?
      return 0.95 if likely_almost_done?
      return merge_request_ratio if relevant_mrs.any?
      return 0.25 if in_dev? || in_progress?
      return 0.7 if in_review?
      return 0.02 if team_assignees.any?
      0.0
    end
  end

  private

  def merge_request_ratio
    relevant_mrs.sum do |mr|
      next 1.0 if mr.state != "opened"
      next 0.2 if only_local_changes?(mr)
      next 0.4 if draft_unlikely_reviewed?(mr)
      next 0.5 if draft_likely_reviewed?(mr)
      next 0.6 if ineligible_for_merge?(mr)
      next 0.7 if waiting_on_review?(mr)

      0.0
    end / relevant_mrs.count
  end

  private

  def likely_complete?
    state == "closed" ||
      status.include?("production") ||
      status.include?("complete")
  end

  def likely_almost_done?
    status.include?("verification") ||
      status.include?("staging") ||
      status.include?("canary")
  end

  def in_progress?
    status.include?("in dev") ||
      status.include?("in review")
  end

  def in_dev?
    status.include?("in dev")
  end

  def in_review?
    status.include?("in review")
  end

  def in_progress?
    planning? && team_assignees.any?
  end

  def ineligible_for_merge?(merge_request)
    merge_request.pipeline.status == "failed" ||
      merge_request.target_branch != "master"
  end

  def draft_unlikely_reviewed?(merge_request)
    merge_request.draft && merge_request.user_notes_count <= 6
  end

  def draft_likely_reviewed?(merge_request)
    merge_request.draft && merge_request.user_notes_count > 6
  end

  def only_local_changes?(merge_request)
    merge_request.pipeline.nil?
  end

  def waiting_on_review?(merge_request)
    merge_request.reviewers.any?
  end
end