# Helper for providing consistently interfaces for
# nested issue lists under provided groupings
class IssueDistribution
  attr_reader :issues, :label, :sub_group_label, :sort

  def initialize(issues, sort: true)
    @issues = issues
    @sort = sort
  end

  def group(&blk)
    @group = blk
  end

  def sub_group(&blk)
    @sub_group = blk
  end

  def grouped
    @grouped ||= sorted(issues.group_by(&@group))
  end

  def sub_grouped
    @sub_grouped ||= grouped.map do |_, sub_issues|
      dist = IssueDistribution.new(sub_issues, sort: sort)
      dist.group(&@sub_group)
      dist
    end
  end

  def group_count
    issues.count
  end

  def sorted(hash)
    return hash unless sort

    hash.sort_by { _2.count }
      .reverse
      .to_h
  end
end