**Quarter goals:** [Q1 goals](https://gitlab.com/gitlab-org/gitlab/-/issues/386883) | **OKRs:**  | **Milestone plans:** [15.9 (**current**)](https://gitlab.com/gitlab-org/gitlab/-/issues/386880) - [15.10](https://gitlab.com/gitlab-org/gitlab/-/issues/386881) - [15.11](https://gitlab.com/gitlab-org/gitlab/-/issues/386882)

**Previous updates:** {-include link to previous update-}

--- 

# 🚦 Async update of what happened in WXX (yyyy-mm-dd to yyyy-mm-dd)

## 💥 Top highlights
- HIGHLIGHT: 

## ⚠️ High impacting risks
- 

# 🛟 Help needed
_Where we need help and from whom_

| From | What |
| ------ | ------ |
|  |  |  

# 3 P's of Organizational Success

_Without good people in the right roles, our group will struggle to serve customers, dampening our potential. If we don't put effective processes in place, our overhead will increase, reducing efficiency. And if we don't have products or services that meet the needs of our market, we won't be for long._


Legend:
👍🏽 going well
👎🏽 not so well
⏭ next steps
🩹 remediation plan
🛟 help or attention needed

## People

- [Team health pulse](https://app.leadhonestly.com/surveys/91)
 

## Process
_Success on a large scale is all about the actions we take to perform a specific function_

- 
 
## Product
_Our product is the most visible part of our business and deserves the lion’s share of our focus_

-

### Reliability and performance indicators

Legend:  
🟢 on track
🟡 needs attention
🔴 at risk
⬆️⬇️ change from last update

Summary:
- 




- 🟢 **[Error budget][error-budget]**: Remaining Xs, availability X%
- 🟢 ~security, ~"bug::vulnerability" [x issues][security-bugs]
- 🟢 ~"bug::availability" [x issues][availability-bugs]
- 🟢 ~infradev [x issues][infradev]
- 🔴 ~"corrective action" [x issues][corrective-actions]
- 🟢 ~"ci-decomposition" [0 issues][cidecomposition]
- 🔴 ~regression [x issues][regression]
- 🟢 ~"type::bug" ~"severity::1" [x bugs][s1-bugs]
- 🔴 ~"type::bug" ~"severity::2" [x bugs][s2-bugs]
- 🟡 MR Rate (current month): [x][dev-dashboard] 
- 🟢 Type of work ratio (current milestone): [% feature, % maintenance, % bug][issues-by-type-dashboard]
- 🟢 MRs without type (current month): [x%][mr-type]
- 🟡 ~"feature flag" older than 2 months: [x feature flags][dev-dashboard] 
- 🔴 Quarantined tests: [x tests][quarantined-tests]



## Michelle's initiatives

* .
* :tada: Completed improvements
  * :efficiency: improvements: 


# Announcements


## 🎯 Priorities for this week

{-copy the goals of the quarter including the why-}

1. :closed_lock_with_key: Security issues
1. Milestone work ([board][board]). Prioritize first any blocker and then the things that will need feature flags.
1. 🤓 Investigation issues
1. Take some time to do [refinement](https://about.gitlab.com/handbook/engineering/development/ops/package/#refinement) of [s2-bugs](https://gitlab.com/dashboard/issues?sort=created_date&state=opened&label_name%5B%5D=type::bug&label_name%5B%5D=group%3A%3Acontainer%20registry&label_name%5B%5D=severity::2)
1. If time allows, take a look at the ~Stretch issues for the milestone



## 👀 What's happening at GitLab
Please make sure to [keep yourself informed](https://about.gitlab.com/handbook/engineering/#keeping-yourself-informed). Following is Michelle's curated list!

:eyes: You really really really want to take a look at these:
- .

_You maybe want to take a look to the following:_
- .

:bookmark: And here there is more interesting content
- .


<details><summary>Admin tasks</summary>

- [ ] Name this issue as "Container Registry engineering async update (yyyy-mm-dd to yyyy-mm-dd)"
- [ ] Update links to previous weeks and current milestone
- [ ] Check for highlights from [team's retrospective](https://docs.google.com/document/d/1gIHCbskdynH6C2xmeCCkwlDnTIYSHjWaeD51MZMByIw/edit)
- [ ] Check the status of the quarter goals epics and issues, including [what are we working on](https://about.gitlab.com/handbook/engineering/development/ops/package/container-registry/#what-are-we-working-on)
- [ ] Get the numbers for the reliability and PIs
- [ ] Write a quick summary for reliability and PIs
- [ ] Collect all help needed in the table
- [ ] List the priorities for the week
- [ ] Get a list of announcements
    - [ ] from Slack with the search `hasmy::pushpin:`
    - [ ] Get a list of [issues](https://gitlab.com/dashboard/issues?scope=all&state=all&my_reaction_emoji=pushpin)
    - [ ] Get a list of epics [in org](https://gitlab.com/groups/gitlab-org/-/epics?state=all&my_reaction_emoji=pushpin) and [in com](https://gitlab.com/groups/gitlab-com/-/epics?state=all&my_reaction_emoji=pushpin) 
    - [ ] Get a list of [MRs](https://gitlab.com/dashboard/merge_requests?scope=all&state=all&my_reaction_emoji=pushpin)
    - [ ] Get a list of announcements from various agendas [planner (goals, number 4)](https://gitlab.com/michelletorres/planner/-/blob/main/.gitlab/issue_templates/week-journal.md#dart-goals)
    - [ ] Get a list of interesting things from Slack with the search `hasmy::bookmark:`
- [ ] Consider adding a message on the #thanks channel for the accomplishments in the last week
- [ ] Post weekly message on slack

</details>

<!-- do not remove -->
[error-budget]: https://dashboards.gitlab.net/d/stage-groups-detail-container_registry/stage-groups-container-registry-group-error-budget-detail?from=now-7d&to=now
[dev-dashboard]: https://app.periscopedata.com/app/gitlab/681347/Development-Embedded-Dashboard
[security-bugs]: https://gitlab.com/dashboard/issues?scope=all&state=opened&label_name[]=group%3A%3Acontainer%20registry&label_name[]=bug%3A%3Avulnerability
[infradev]: https://gitlab.com/dashboard/issues?scope=all&state=opened&sort=created_date&label_name[]=group%3A%3Acontainer%20registry&label_name[]=infradev
[cidecomposition]: https://gitlab.com/dashboard/issues?sort=created_date&state=opened&label_name[]=group%3A%3Acontainer%20registry&label_name[]=ci-decomposition
[availability-bugs]: https://gitlab.com/dashboard/issues?sort=created_date&state=opened&label_name[]=group%3A%3Acontainer%20registry&label_name[]=bug::availability
[corrective-actions]: https://gitlab.com/dashboard/issues?sort=created_date&state=opened&label_name[]=group%3A%3Acontainer%20registry&label_name[]=corrective+action
[regression]: https://gitlab.com/dashboard/issues?sort=created_date&state=opened&label_name[]=group%3A%3Acontainer%20registry&label_name[]=regression
[s1-bugs]: https://gitlab.com/dashboard/issues?sort=created_date&state=opened&label_name[]=type::bug&label_name[]=group%3A%3Acontainer%20registry&label_name[]=severity::1
[s2-bugs]: https://gitlab.com/dashboard/issues?sort=created_date&state=opened&label_name[]=type::bug&label_name[]=group%3A%3Acontainer%20registry&label_name[]=severity::2
[quarantined-tests]: https://gitlab.com/dashboard/issues?sort=created_date&scope=all&state=opened&label_name[]=group%3A%3Acontainer%20registry&label_name[]=quarantined%20test
[community-contributions]: https://gitlab.com/dashboard/merge_requests?scope=all&state=opened&label_name[]=Community%20contribution&label_name[]=group%3A%3Acontainer%20registry
[idle-cc]: https://gitlab.com/dashboard/merge_requests?scope=all&state=opened&label_name[]=Community%20contribution&label_name[]=group%3A%3Acontainer%20registry&label_name[]=idle
[mr-type]: https://app.periscopedata.com/app/gitlab/976854/Merge-Request-Types-Detail
[issues-by-type-dashboard]: https://app.periscopedata.com/app/gitlab/1042933/Issue-Types-by-Milestone
[mr-rate]: https://app.periscopedata.com/app/gitlab/1065359/Package-MR-rate-analysis
[flaky-tests]: https://gitlab.com/gitlab-org/container-registry/-/issues/?sort=created_date&state=opened&label_name%5B%5D=failure%3A%3Aflaky-test
[board]: https://gitlab.com/groups/gitlab-org/-/boards/1284221?label_name[]=group%3A%3Acontainer%20registry&milestone_title={-replace milestone-}

/assign @michelletorres
/due next friday
/epic https://gitlab.com/groups/gitlab-org/ci-cd/package-stage/-/epics/10
/label ~"section::ops" ~"devops::package" ~"Continuous Delivery" ~"FY24::Q1" ~"OpsSection::AsyncUpdate" ~"group::container registry"

